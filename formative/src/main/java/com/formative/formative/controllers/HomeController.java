package com.formative.formative.controllers;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import java.time.LocalDate;
@Controller
@RequestMapping("/")
public class HomeController{

    private LocalDate date = LocalDate.now(); 
    private String test = "halo";



    @GetMapping("/")
    public String showHome(Model model){
        
        return "index";
    }

    @GetMapping("/result")
    public String showResult(Model model){
        model.addAttribute("date", date);
        return "result";
    }

}
