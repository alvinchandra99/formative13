package com.formative.formative.controllers;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import java.util.ArrayList;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class HomeController{





    @GetMapping("/")
    public String showHome(Model model){

        ArrayList<PersonPojo> listPerson = new ArrayList<PersonPojo>();
        listPerson.add(new PersonPojo("Jokowi", "Widodo", "a@gmail.com"));
        listPerson.add(new PersonPojo("Basuki", "Purnama", "b@gmail.com"));
        listPerson.add(new PersonPojo("Prabowo", "Subianto", "c@gmail.com"));
        listPerson.add(new PersonPojo("Nadiem", "Makarim", "d@gmail.com"));
        listPerson.add(new PersonPojo("Sri", "Mulyani", "e@gmail.com"));
        listPerson.add(new PersonPojo("Najwa", "Shihab", "f@gmail.com"));
        listPerson.add(new PersonPojo("Budi", "Hartono", "g@gmail.com"));
        listPerson.add(new PersonPojo("Arthur", "Marunduh", "h@gmail.com"));
        listPerson.add(new PersonPojo("Erick", "Thohir", "i@gmail.com"));
        listPerson.add(new PersonPojo("William", "Tanuwijaya", "j@gmail.com"));
      

        model.addAttribute("listPerson", listPerson);

        return "index";
    }


}
