<%@ page contentType="text/html;charset=UTF-8" language="java" %> <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
  </head>
  <body>
    <table>
      <thead>
        <tr>
          <th>FirstName</th>
          <th>LastName</th>
          <th>Email</th>
        </tr>
      </thead>
      <tbody>
        <c:forEach items="${listPerson}" var="person">
          <tr>
            <td>${person.getFirstName()}</td>
            <td>${person.getLastName()}</td>
            <td>${person.getEmail()}</td>
          </tr>
        </c:forEach>
      </tbody>
    </table>
  </body>
</html>
